# Terraform cloud modules

Just a few scripts:
* `tf_tags.py` List all versions of a Terraform module or a group of them
* `tf_modules_update.py` Check what Terraform modules are used in your source code and their latest versions
