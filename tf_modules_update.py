#!/usr/bin/env python

import gitlab
import os
import sys
import getpass
import argparse
import re
from pkg_resources import packaging

GITLAB_URL = 'https://www.gitlab.com'


def find_modules(directory):
    """ Find Terraform modules under a directory """

    module = tuple()
    modules = list(module)
    file_modules = dict()
    modules_found = list(file_modules)

    # Find Terraform files
    tf_files = find_tf_files(directory)

    for file in tf_files:
        file_modules = dict()
        modules = extract_modules(file)
        if modules:
            file_modules[os.path.relpath(file, directory)] = modules
            modules_found.append(file_modules)

    return modules_found


def find_tf_files(directory):
    """ Find Terraform files under a directory"""

    files_found = list()
    for root, dirs, files in os.walk(directory):
        for filename in files:
            if filename.endswith('.tf'):
                files_found.append(os.path.join(root, filename))
    return files_found


def extract_modules(filename):
    """ Extract modules from Terraform files"""

    with open(filename, 'r') as f:
        module = tuple()
        modules_used = list(module)

        for line in f:

            # Get module's name
            module_name = str()
            try:
                module_name = re.search('module "(.+?)"', line).group(1)
            except AttributeError:
                pass

            if module_name:
                module = (module_name,)

                # Get module's source
                module_source = str()
                try:
                    while not module_source:
                        line = f.readline()
                        module_source = re.search(
                                'source.*= "(.+?)"', line
                            ).group(1)
                except AttributeError:
                    pass

                if module_source and (
                            "app.terraform.io/wallbox" in module_source
                        ):
                    source_substrings = module_source.split('/')

                    if len(source_substrings) < 3:
                        raise AttributeError('Module source is wrong')

                    composed_module = (
                        f'terraform-'
                        f'{source_substrings[3]}-'
                        f'{source_substrings[2]}'
                    )

                    module = module + (composed_module,)

                    # Get module's version
                    module_version = str()
                    try:
                        while not module_version:
                            line = f.readline()
                            module_version = re.search(
                                    'version.*= "(.+?)"', line
                                ).group(1)
                    except AttributeError:
                        pass

                    if module_version:
                        module = module + (module_version,)

                    if len(module) == 3:
                        modules_used.append(module)
                    else:
                        raise AttributeError(
                                'Could not find all information of the module'
                            )

    return modules_used


def main(args):

    # If path argument not present, defaults to current path
    if not args.path:
        directory = os.getcwd()
    else:
        directory = args.path

    # Ask for a token if it was not present in the arguments
    if not args.token:
        TOKEN = getpass.getpass('Enter TOKEN: ')
    else:
        TOKEN = args.token

    if not TOKEN:
        print('You must enter a valid TOKEN')
        sys.exit(1)

    # Find modules used in Terraform files
    modules_used = find_modules(directory)

    if modules_used:

        # Create a list of modules names without duplicated ones
        module_names = list()
        for file in modules_used:
            for _, modules in file.items():
                [module_names.append(x[1])
                 for x in modules
                 if x[1] not in module_names]

        # Get all projects under Terraform-modules subgroup
        gl = gitlab.Gitlab(GITLAB_URL, TOKEN, api_version=4)
        group = gl.groups.get('8509598')
        projects = group.projects.list(all=True)

        # Get latest versions of the modules used
        lastest_versions = dict()
        for project in projects:
            if project.path in module_names:
                project_obj = gl.projects.get(project.id)
                tag_list = project_obj.tags.list(all=False)

                # Assume first tag retrieved is the last tag present in repo
                last_tag = tag_list[0].name
                lastest_versions[project.path] = last_tag

        for file in modules_used:
            for file_path, modules in file.items():
                print(f'\n\033[1m{file_path}\033[0m')
                for module in modules:
                    if (packaging.version.parse(module[2]) <
                            packaging.version.parse(
                                lastest_versions[module[1]]
                            )):
                        print(f'\033[96m{module[1]:40}\033[0m \t '
                              f'\033[91m{module[2]}\033[0m '
                              f'\033[92m--> '
                              f'{lastest_versions[module[1]]}\033[0m')
    else:
        print(f'No modules found from Terraform registry in {directory}')
        sys.exit(1)


if __name__ == '__main__':
    # Parse command arguments
    parser = argparse.ArgumentParser(description='Show lastest Terraform \
                                                  module versions')
    parser.add_argument('path', metavar='path', type=str,
                        nargs='?', default=None,
                        help="Path to search. Defaults to current path")
    parser.add_argument('-t', '--token', type=str,
                        help='Gitlab TOKEN')

    args = parser.parse_args()

    main(args)
