#!/usr/bin/env python

import gitlab
import sys
import getpass
import argparse

GITLAB_URL = 'https://www.gitlab.com'


def main(args):

    # Ask for a token if it was not present in the arguments
    if not args.token:
        TOKEN = getpass.getpass('Enter TOKEN: ')
    else:
        TOKEN = args.token

    if not TOKEN:
        print('You must enter a valid TOKEN')
        sys.exit(1)

    gl = gitlab.Gitlab(GITLAB_URL, TOKEN, api_version=4)

    group = gl.groups.get('8509598')            # Terraform-modules project
    projects = group.projects.list(all=True)

    if args.project:
        for project in projects:
            if args.project in project.path:
                pass

    for project in projects:
        if args.project and args.project not in project.path:
            continue

        project_obj = gl.projects.get(project.id)
        tag_list = project_obj.tags.list(all=True)
        tags = [tag.name for tag in tag_list]

        print(f'{project.path} = {tags}')


if __name__ == '__main__':
    # Parse command arguments
    parser = argparse.ArgumentParser(description='Show all module versions')
    parser.add_argument("-p", "--project", type=str,
                        help="Project name")
    parser.add_argument('-t', '--token', type=str,
                        help='Gitlab TOKEN')

    args = parser.parse_args()

    main(args)
